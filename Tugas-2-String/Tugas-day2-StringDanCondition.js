// TUGAS A : STRING

// Soal 1
var first = 'javascript ';
var second = 'is ';
var third = 'awesome ';
var fourth = 'and ';
var fifth = 'I ';
var sixth = 'Love ';
var seventh = 'it ';

console.log(first.concat(second.concat(third.concat(fourth.concat(fifth.concat(sixth.concat(seventh)))))));

//Soal 2
var sentence  	= 'I am going to be React Native Developer';
var firstword 	= sentence[0];
var secondword 	= sentence[2] + sentence[3];
var thirdword 	= sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthword 	= sentence[11] + sentence[12];
var fifthword 	= sentence[14] + sentence[15];
var sixthword 	= sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var seventhword = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var eightword 	= sentence[29] + sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log('First word   : ' + firstword);
console.log('Second word  : ' + secondword);
console.log('Third word   : ' + thirdword);
console.log('Fourth word  : ' + fourthword);
console.log('Fifth word   : ' + fifthword);
console.log('Sixth word   : ' + sixthword);
console.log('Seventh word : ' + seventhword);
console.log('eighth word  : ' + eightword);

//Soal 3
//x		 01234567890123456789012345
//y              12345678901234567890123456
var sentence2 = 'wow Javascript is so cool';
var firstword2 = sentence2.substring(0,3);
var secondword2 = sentence2.substring(4,14);
var thirdword2 = sentence2.substring(15,17);
var fourthword2 = sentence2.substring(18,20);
var fifthword2 = sentence2.substring(21,25)
console.log('First word  : ' + firstword2);
console.log('Second word : ' + secondword2);
console.log('Third word  : ' + thirdword2);
console.log('Fourth word : ' + fourthword2);
console.log('Fifth word  : ' + fifthword2);

//Soal 4

var sentence3 = 'wow Javascript is so cool';

var firstword3 = sentence3.substring(0,3);
var secondword3 = sentence3.substring(4,14);
var thirdword3 = sentence3.substring(15,17);
var fourthword3 = sentence3.substring(18,20);
var fifthword3 = sentence3.substring(21,25);

var firstwordlength3 = firstword3.length
var secondwordlength3 = secondword3.length
var thirdwordlength3 = thirdword3.length
var fourthwordlength3 = fourthword3.length
var fifthwordlength3 = fifthword3.length

console.log('First word  : ' + firstword3 + ' with length: ' + firstwordlength3);
console.log('Second word : ' + secondword3 + ' with length: ' + secondwordlength3);
console.log('third word  : ' + thirdword3 + ' with length: ' + thirdwordlength3);
console.log('fourth word : ' + fourthword3 + ' with length: ' + fourthwordlength3);
console.log('fifth word  : ' + fifthword3 + ' with length: ' + fifthwordlength3);

// TUGAS B : CONDITIONAL

//IF-ELSE
var prompt = require('prompt-sync')();

const nama = prompt('masukkan nama Anda : ');
if (nama) {
console.log(`Selamat datang di dunia werewolf, ${nama}`);
}
else {
	nama = prompt('isikan nama Anda ! : ');
}

const peran = prompt('masukkan peran yang Anda inginkan (werewolf, guard, penyihir): ');

if (peran) {
 	if (peran == "werewolf") {
 	console.log(`halo ${peran} ${nama}, kamu akan memakan mangsa setiap malam`);
 	} else if (peran == "guard") {
	console.log(`halo ${peran} ${nama}, kamu akan melindungi temanmu dari serangan werewolf`);
	} else if (peran == "penyihir") {
	console.log(`halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf`);
	} else {
	console.log(`halo ${nama}, maaf, peran yang anda minta tidak tersedia !`);
	}


} else  {
	const peran = prompt(`Halo ${nama} masukkan peranmu untuk memulai (werewolf, guard, penyihir): `);
	if (peran == "werewolf") {
	console.log(`halo ${peran} ${nama}, kamu akan memakan mangsa setiap malam`);
	} else if (peran == "guard") {
	console.log(`halo ${peran} ${nama}, kamu akan melindungi temanmu dari serangan werewolf`);
	} else {
	console.log(`halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf`);
	}
}


//SWITCH CASE

var hari = Number(prompt('masukkan hari 1-31       : '));
var bulan= Number(prompt('masukkan bulan 1-12      : '));
var tahun= Number(prompt('masukkan tahun 1990-2030 : '));

switch(bulan) {
	case 1: var string = 'januari'; break;
	case 2: var string = 'februari'; break;
	case 3: var string = 'maret'; break;
	case 4: var string = 'april'; break;	
	case 5: var string = 'mei'; break;
	case 6: var string = 'juni'; break;
	case 7: var string = 'juli'; break;
	case 8: var string = 'agustus'; break;
	case 9: var string = 'september'; break;
	case 10: var string = 'oktober'; break;
	case 11: var string = 'november'; break;
	case 12: var string = 'desember'; break;
}

console.log(`${hari} ${string} ${tahun}`);

//SPEKULASI
var a = Number(prompt('tekan tombol(1-4): '));
switch(a) {
	case 1:	{console.log('Alhamdulillah'); break; }
	case 2: {console.log('Terimakasih Sanbercode');break;}
	case 3: {console.log('Thanks Sanbercode'); break;}
	case 4: {console.log('jazakallah, Sanbercode'); break;}
	default: {console.log('شكرا'); break;}}

