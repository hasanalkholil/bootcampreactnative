// TUGAS A : STRING

// Soal 1
var first = 'javascript ';
var second = 'is ';
var third = 'awesome ';
var fourth = 'and ';
var fifth = 'I ';
var sixth = 'Love ';
var seventh = 'it ';

console.log(first.concat(second.concat(third.concat(fourth.concat(fifth.concat(sixth.concat(seventh)))))));

//Soal 2
var sentence  	= 'I am going to be React Native Developer';
var firstword 	= sentence[0];
var secondword 	= sentence[2] + sentence[3];
var thirdword 	= sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthword 	= sentence[11] + sentence[12];
var fifthword 	= sentence[14] + sentence[15];
var sixthword 	= sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21];
var seventhword = sentence[23] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28];
var eightword 	= sentence[29] + sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38];

console.log('First word   : ' + firstword);
console.log('Second word  : ' + secondword);
console.log('Third word   : ' + thirdword);
console.log('Fourth word  : ' + fourthword);
console.log('Fifth word   : ' + fifthword);
console.log('Sixth word   : ' + sixthword);
console.log('Seventh word : ' + seventhword);
console.log('eighth word  : ' + eightword);

//Soal 3
//x		 01234567890123456789012345
//y              12345678901234567890123456
var sentence2 = 'wow Javascript is so cool';
var firstword2 = sentence2.substring(0,3);
var secondword2 = sentence2.substring(4,14);
var thirdword2 = sentence2.substring(15,17);
var fourthword2 = sentence2.substring(18,20);
var fifthword2 = sentence2.substring(21,25)
console.log('First word  : ' + firstword2);
console.log('Second word : ' + secondword2);
console.log('Third word  : ' + thirdword2);
console.log('Fourth word : ' + fourthword2);
console.log('Fifth word  : ' + fifthword2);

//Soal 4

var sentence3 = 'wow Javascript is so cool';

var firstword3 = sentence3.substring(0,3);
var secondword3 = sentence3.substring(4,14);
var thirdword3 = sentence3.substring(15,17);
var fourthword3 = sentence3.substring(18,20);
var fifthword3 = sentence3.substring(21,25);

var firstwordlength3 = firstword3.length
var secondwordlength3 = secondword3.length
var thirdwordlength3 = thirdword3.length
var fourthwordlength3 = fourthword3.length
var fifthwordlength3 = fifthword3.length

console.log('First word  : ' + firstword3 + ' with length: ' + firstwordlength3);
console.log('Second word : ' + secondword3 + ' with length: ' + secondwordlength3);
console.log('third word  : ' + thirdword3 + ' with length: ' + thirdwordlength3);
console.log('fourth word : ' + fourthword3 + ' with length: ' + fourthwordlength3);
console.log('fifth word  : ' + fifthword3 + ' with length: ' + fifthwordlength3);

// TUGAS B : CONDITIONAL

//IF-ELSE
var prompt = require('prompt-sync')();

const nama = prompt('masukkan nama Anda : ');
console.log(`Selamat datang di dunia werewolf, ${nama}`);
const peran = prompt('masukkan peran yang Anda inginkan (werewolf, guard, penyihir): ');

if (peran) {
 	if (peran == "werewolf") {
 	console.log(`halo ${peran} ${nama}, kamu akan memakan mangsa setiap malam`);
 	} else if (peran == "guard") {
	console.log(`halo ${peran} ${nama}, kamu akan melindungi temanmu dari serangan werewolf`);
	} else {
	console.log(`halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf`);
	}


} else  {
	const peran = prompt(`Halo ${nama} masukkan peranmu untuk memulai (werewolf, guard, penyihir): `);
	if (peran == "werewolf") {
	console.log(`halo ${peran} ${nama}, kamu akan memakan mangsa setiap malam`);
	} else if (peran == "guard") {
	console.log(`halo ${peran} ${nama}, kamu akan melindungi temanmu dari serangan werewolf`);
	} else {
	console.log(`halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf`);
	}
}


//SWITCH CASE

var hari = 21;
var bulan= 1;
var tahun=1945;

var buttonPushed = 1;
switch(buttonPushed) {
	case 1:	{console.log('matikan TV!'); break; }
	case 2: {console.log('turunkan volume TV!');break;}
	case 3: {console.log('tingkatkan volume TV!'); break;}
	case 4: {console.log('matikan suara!'); break;}
	case 5: {console.log('tidak terjadi apa apa'); break;}}
