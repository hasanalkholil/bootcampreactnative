var deret = 2;
var jumlah = 0;
console.log('NO 1 LOOPING WHILE');
console.log('==================');
console.log(' ');
console.log('LOOPING PERTAMA');
console.log(' ');
while(jumlah < 20) { 
  jumlah = jumlah + deret; 
  
  console.log(jumlah + ' I love coding ');
}


var deret2 = 2;
var jumlah2 = 20;
console.log(' ');
console.log(' ');
console.log('LOOPING KEDUA');
console.log(' ');
while(jumlah2 > 0) {

    console.log(jumlah2 + ' I will become a mobile developer');
    jumlah2 = jumlah2 - deret2;
}

console.log(' ');
console.log(' ');
console.log('NO 2 - LOOPING FOR');
console.log('==================');
console.log(' ');

var jum = 0;
for(var i=1; i<=20; i++){
    jum++;
        if (jum%2==0){
            var a = 'Santai';
        }
        else {
            var a = 'Berkualitas';
            if(jum%3==0){
                var a = 'I LOVE CODING'
            }
        }

    console.log('Jumlah saat ini: ' + jum +' '+ a);
}

console.log(' ');
console.log(' ');
console.log('NO 3 - MEMBUAT PERSEGI PANJANG   (dimensi 8x4)');
console.log('==============================');
console.log(' ');

//var x = 0;
console.log(' ');
console.log('versi 1');
console.log(' ');
for(var i=0;i<4;i++) {
    console.log('########');
}

console.log(' ');
console.log('versi 2');
console.log(' ');
i = 1;
j = 1;

var panjang = 8;
var lebar = 4;
var papan = ' ';

for(j=1 ; j<=lebar;j++){
   
        for(i=1;i<=panjang; i++){
            papan += '#'
       }

       console.log(papan);
    //papan = ' ';
}

console.log(' ');
console.log(' ');
console.log('NO 4 - MEMBUAT TANGGA   (tinggi 7 & alas 7)');
console.log('=====================');
console.log(' ');


console.log(' ');
i = 1;
j = 1;

var x = 1;
var sisi = 7;
var papan = ' ';

for(j=1 ; j<=sisi;j++){
   
        for(i=1;i<=x; i++){
            papan += '#'
       }

       console.log(papan);
    papan = ' ';
}

console.log(' ');
console.log(' ');
console.log('NO 5 - MEMBUAT PAPAN CATUR   (8 X 8)');
console.log('==========================');
console.log(' ');

i = 1;
j = 1;

var panjang = 100;
var lebar = 100;
var papan = ' ';

for(j=1 ; j<=lebar;j++){
    if(j%2==1){
        for(i=1;i<=panjang; i++){
            if(i%2 ==1){
                papan = papan + ' '
            } else{
                papan += '#'
            }
        }
    }else{
        for(i=1; i<=panjang; i++){
            if(i%2==1){
                papan +='#'
            }else {
                papan +=' '
            }
        }
    }
    console.log(papan);
    papan = ' ';
}


console.log(' ');
console.log(' ');
console.log(' ');

i = 1;
j = 1;

var panjang = 8;
var lebar = 8;
var papan = ' ';

for(j=1 ; j<=lebar;j++){
    if(j%2==1){
        for(i=1;i<=panjang; i++){
            if(i%2 ==1){
                papan = papan + ' '
            } else{
                papan += '#'
            }
        }
    }else{
        for(i=1; i<=panjang; i++){
            if(i%2==1){
                papan +='#'
            }else {
                papan +=' '
            }
        }
    }
    console.log(papan);
    papan = ' ';
}