console.log(' ');
console.log('=========');
console.log('SOAL NO 1 - Range');
console.log('=========');
console.log(' ');

function range(begin, end) {
    var array = [];
    if (!end || !begin) {
        return -1;
    } else if (begin > end) {
        for (var i=begin; i>=end; i--) {
            array.push(i);
        }
        return array;
    } else {
        for (var i = begin; i <= end; i++) {
            array.push(i);
        }
        return array;
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1


console.log(' ');
console.log('=========');
console.log('SOAL NO 2 - RAnge with Step');
console.log('=========');
console.log(' ');
function rangeWithStep(startNum, finishNum, step) {
    var arr = [];
    if (startNum == null || finishNum == null) {
        return -1;
    }
    else if (startNum > finishNum) {
        while (startNum >= finishNum) {
            arr.push(startNum);
            startNum -= step;
        }
        return arr;
    }
    else {
        while (startNum <= finishNum) {
            arr.push(startNum);
            startNum += step;
        }
        return arr;
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]


console.log(' ');
console.log('=========');
console.log('SOAL NO 2 - Sum of Range');
console.log('=========');
console.log(' ');

function sum(startNum, finishNum, step) {
    var result;
    if (finishNum == null) {
        if (startNum == null) {
            return 0;
        }
        else {
            return startNum;
        }
    }
    else if (step == null) {
        var tempArr = range(startNum, finishNum);
        result = tempArr.reduce(function (a, b) {
            return a + b;
        }, 0);
        return result;
    }
    else {
        var tempArr = rangeWithStep(startNum, finishNum, step);
        result = tempArr.reduce(function (a, b) {
            return a + b;
        }, 0);
        return result;
    }
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0


console.log(' ');
console.log('=========');
console.log('SOAL NO 4 - Multidimension Array');
console.log('=========');
console.log(' ');
function dataHandling(inputArr) {
    for (x = 0; x < inputArr.length; x++) {
        console.log("Nomor ID: " + inputArr[x][0])
        console.log("Nama Lengkap: " + inputArr[x][1])
        console.log("TTL: " + inputArr[x][2] + ", " + inputArr[x][3])
        console.log("Hobi: " + inputArr[x][4] + "\n")
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input);


console.log(' ');
console.log('=========');
console.log('SOAL NO 5 - Balik Kata');
console.log('=========');
console.log(' ');
function balikKata(kata) {
    var balik = "";
    for (x = kata.length - 1; x >= 0; x--) {
        balik += kata[x];
    }
    return balik;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log(' ');
console.log('=========');
console.log('SOAL NO 6 - Metode Array');
console.log('=========');
console.log(' ');
function dataHandling2(inputArr) {
    //modify array element
    inputArr.splice(1, 2, inputArr[1] + "Elsharawy", "Provinsi " + inputArr[2])

    //add and delete array element with splice
    inputArr.splice(4, 1, "Pria", "SMA Internasional Metro")

    //output modif result
    console.log(inputArr);

    //splitting tanggal
    var tgl = inputArr[3].split("/");

    //switch-case string tanggal
    switch (tgl[1]) {
        case '01': { console.log('Januari'); break; }
        case '02': { console.log('Februari'); break; }
        case '03': { console.log('Maret'); break; }
        case '04': { console.log('April'); break; }
        case '05': { console.log('Mei'); break; }
        case '06': { console.log('Juni'); break; }
        case '07': { console.log('Juli'); break; }
        case '08': { console.log('Agustus'); break; }
        case '09': { console.log('September'); break; }
        case '10': { console.log('Oktober'); break; }
        case '11': { console.log('November'); break; }
        case '12': { console.log('Desember'); break; }
        default: { console.log('Bulan yang dimasukkan harus antara 1 - 12'); }
    }

    //sorting tgl

    var sortTgl = tgl.slice().sort(function (value1, value2) { return value2 - value1 });
    console.log(sortTgl);

    //join tgl
    var joinTgl = tgl.join("-");
    console.log(joinTgl);

    //string 15 max output with slice
    var maxName = inputArr[1].slice(0, 14);
    console.log(maxName);
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
